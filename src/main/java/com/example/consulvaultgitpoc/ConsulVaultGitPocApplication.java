package com.example.consulvaultgitpoc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsulVaultGitPocApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsulVaultGitPocApplication.class, args);
    }

}
