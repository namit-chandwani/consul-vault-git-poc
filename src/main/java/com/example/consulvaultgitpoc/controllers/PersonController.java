package com.example.consulvaultgitpoc.controllers;

import com.example.consulvaultgitpoc.configuration.AppConfig;
import com.example.consulvaultgitpoc.configuration.SecretConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private AppConfig appConfig;

    @Autowired
    private SecretConfig secretConfig;

    @GetMapping("/details")
    public String getPersonDetails() {
        return appConfig.toString()+"\n"+secretConfig.toString();
    }
}
