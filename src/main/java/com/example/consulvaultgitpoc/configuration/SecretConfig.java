package com.example.consulvaultgitpoc.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class SecretConfig {
    @Value("${username}")
    private String username;

    @Value("${password}")
    private String password;
}
